import { IsNotEmpty, Length } from 'class-validator';
import { UserDto } from '../../user/DTO/user.dto';

export class SignUpUserDTO extends UserDto {
  @IsNotEmpty()
  login: string;

  @Length(3, 10)
  @IsNotEmpty()
  password: string;
}