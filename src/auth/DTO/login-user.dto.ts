import { UserDto } from '../../user/DTO/user.dto';
import { IsNotEmpty } from 'class-validator';

export class LoginUserDto extends UserDto {
  @IsNotEmpty()
  login: string;

  @IsNotEmpty()
  password: string;
}