import { Injectable, NotFoundException } from '@nestjs/common';
import { SignUpUserDTO } from './DTO/sign-up-user.dto';
import { IUserTokenBody } from './interfaces/user-token-body.interface';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { LoginUserDto } from './DTO/login-user.dto';

const bcrypt = require('bcrypt');

@Injectable()
export class AuthService {

  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {
  }

  async signUp(userDTO: SignUpUserDTO): Promise<IUserTokenBody> {
    const user = await this.userService.createUser(userDTO);
    return this.signToken(user);
  }

  async login(userDTO: LoginUserDto) {
    const user = await this.validateUser(userDTO.login, userDTO.password);
    if (!user) {
      throw new NotFoundException();
    }
    return this.signToken(user);
  }

  async validateUser(login, password) {
    const user = await this.userService.findOneByLogin(login);
    if (!user || !(user && await bcrypt.compare(password, user.password))) {
      return false;
    }
    return user;
  }

  private async signToken(data: any): Promise<IUserTokenBody> {
    const { password, ...payload } = data;
    return {
      token: this.jwtService.sign(payload),
    };
  }
}
