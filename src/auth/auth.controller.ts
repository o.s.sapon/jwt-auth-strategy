import {
  Body,
  Controller,
  Post,
} from '@nestjs/common';
import { SignUpUserDTO } from './DTO/sign-up-user.dto';
import { AuthService } from './auth.service';
import { IUserTokenBody } from './interfaces/user-token-body.interface';
import { LoginUserDto } from './DTO/login-user.dto';

@Controller('auth')
export class AuthController {

  constructor(
    private readonly authService: AuthService
  ) {
  }

  @Post('/sign-up')
  async handleSignUp(@Body() user: SignUpUserDTO) : Promise<IUserTokenBody> {
    return this.authService.signUp(user)
  }

  @Post('/login')
  async handleLogin(@Body() user: LoginUserDto) {
    return this.authService.login(user);
  }

}
