import { IsOptional } from 'class-validator';

export class UserDto {
  @IsOptional()
  id: number;

  @IsOptional()
  login: string

  @IsOptional()
  password: string

  @IsOptional()
  createdAt: Date

  @IsOptional()
  updatedAt: Date
}