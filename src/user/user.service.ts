import { HttpException, Injectable } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { UserEntity } from './entities/user.entity';
import { SignUpUserDTO } from '../auth/DTO/sign-up-user.dto';

@Injectable()
export class UserService {

  constructor(
    private readonly userRepository: UserRepository,
  ) {
  }

  async findOneByLogin(login: string) {
    return this.userRepository.findOne({
      where: {
        login,
      },
    });
  }

  async createUser(userDTO: SignUpUserDTO) {
    const user = await this.findOneByLogin(userDTO.login);
    if (user) {
      throw new HttpException('User already exists', 409);
    }
    const newUser = new UserEntity();
    newUser.login = userDTO.login;
    newUser.password = userDTO.password;
    await this.userRepository.save(newUser);
    return newUser;
  }
}