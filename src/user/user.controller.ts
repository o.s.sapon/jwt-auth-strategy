import {
  Controller,
  Post,
  UseGuards,
  Request,
} from '@nestjs/common';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { UserDto } from './DTO/user.dto';

@Controller('user')
export class UserController {

  @UseGuards(JwtAuthGuard)
  @Post('/me')
  me(@Request() req) : UserDto  {
    return req.user;
  }

}
